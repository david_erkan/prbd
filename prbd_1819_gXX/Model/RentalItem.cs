﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prbd_1819_gXX
{
    class RentalItem : EntityBase<MySqlModel>
    {
        [Key]
        public int RentalItemId { get; set; }
        public DateTime? ReturnDate { get; set; }

    }
}
