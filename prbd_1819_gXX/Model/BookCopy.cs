﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prbd_1819_gXX
{
    class BookCopy : EntityBase<MySqlModel>
    {
        [Key]
        public int BookCopyId { get; set; }
        public DateTime AquisitionDate { get; set; }
        public User RentedBy { get; private set; }
    }
}
