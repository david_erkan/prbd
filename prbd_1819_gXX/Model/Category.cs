﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prbd_1819_gXX
{
    class Category : EntityBase<MySqlModel>
    {
        [Key]
        public int CategoryId { get; set; }
        public string Name { get; set; }
    }
}
