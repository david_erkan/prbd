﻿using PRBD_Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prbd_1819_gXX
{
    enum Role
    {
        User,
        Admin,
        Manager,
    }

    class User : EntityBase<MySqlModel>
    {
        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public Role Role{ get; set; }
        public RentalItem[] ActiveRentalItems { get; }
        public int Age { get; }
    }
}
